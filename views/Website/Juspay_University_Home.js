const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const ImageView = require("@juspay/mystique-backend").views.ImageView;
const TextView = require("@juspay/mystique-backend").views.TextView;

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Website/Juspay_University_Home');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class Juspay_University_Home extends Controller {

	constructor(props, children, state) {
		super(props, children);
		this.shouldCacheScreen = true;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				padding="27,59,85,87"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				clickable="true"
				accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_JUSPAYUNIVERSITY}
				style={this.style_JuspayUniversity}>
				<LinearLayout
					id={this.idSet.Header}
					height="50"
					width="match_parent"
					orientation="horizontal"
					margin="73,0,15,0"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_HEADER}
					style={this.style_Header}>
					<ImageView
						id={this.idSet.LogoLeft}
						height="40"
						width="334"
						orientation="horizontal"
						margin="0,6,0,0"
						imageUrl="logoleft17"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_LOGOLEFT}
						style={this.style_LogoLeft} />
					<LinearLayout
						id={this.idSet.Space2}
						weight="1"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE2}
						style={this.style_Space2} />
					<LinearLayout
						id={this.idSet.HeaderRight}
						height="50"
						width="517"
						orientation="horizontal"
						gravity="center_vertical"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_HEADERRIGHT}
						style={this.style_HeaderRight}>
						<TextView
							id={this.idSet.Projects}
							height="33"
							width="87"
							textSize="24"
							color="#ff3a3a3a"
							fontStyle="Avenir-Roman"
							gravity="center"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_PROJECTS}
							text={STR.JUSPAY_UNIVERSITY_HOME_PROJECTS}
							style={this.style_Projects} />
						<TextView
							id={this.idSet.Forum}
							height="33"
							width="70"
							margin="80,0,0,0"
							textSize="24"
							color="#ff3a3a3a"
							fontStyle="Avenir-Roman"
							gravity="center"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_FORUM}
							text={STR.JUSPAY_UNIVERSITY_HOME_FORUM}
							style={this.style_Forum} />
						<LinearLayout
							id={this.idSet.Space3}
							weight="1"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE3}
							style={this.style_Space3} />
						<LinearLayout
							id={this.idSet.Start_Button}
							height="50"
							width="200"
							orientation="vertical"
							padding="43,9,41,8"
							background="#ffee0000"
							cornerRadius="4"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_START_BUTTON}
							style={this.style_Start_Button}>
							<TextView
								id={this.idSet.Let_s_Start}
								height="33"
								width="116"
								textSize="24"
								color="#ffffffff"
								fontStyle="Avenir-Heavy"
								gravity="center"
								accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_LET_S_START}
								text={STR.JUSPAY_UNIVERSITY_HOME_LET_S_START}
								style={this.style_Let_s_Start} />
						</LinearLayout>
					</LinearLayout>
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Hero___above_the_fold}
					height="631"
					width="1328"
					orientation="horizontal"
					margin="0,96,0,0"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_HERO___ABOVE_THE_FOLD}
					style={this.style_Hero___above_the_fold}>
					<ImageView
						id={this.idSet.Intro_Video}
						height="631"
						width="682"
						imageUrl="introvideo16"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_INTRO_VIDEO}
						style={this.style_Intro_Video} />
					<LinearLayout
						id={this.idSet.IntroRight}
						height="398"
						width="635"
						orientation="vertical"
						margin="11,104,0,0"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_INTRORIGHT}
						style={this.style_IntroRight}>
						<TextView
							id={this.idSet.Hero_Epic}
							height="144"
							width="match_parent"
							textSize="60"
							color="#ff000000"
							fontStyle="Avenir-Black"
							lineHeight="72px"
							gravity="left"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_HERO_EPIC}
							text={STR.JUSPAY_UNIVERSITY_HOME_HERO_EPIC}
							style={this.style_Hero_Epic} />
						<TextView
							id={this.idSet.Section_body}
							height="224"
							width="600"
							margin="0,30,0,0"
							textSize="18"
							color="#ff3a3a3a"
							fontStyle="RobotoMono-Regular"
							lineHeight="32px"
							gravity="left"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SECTION_BODY}
							text={STR.JUSPAY_UNIVERSITY_HOME_SECTION_BODY}
							style={this.style_Section_body} />
					</LinearLayout>
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Presto_Section}
					height="690"
					width="1158"
					orientation="vertical"
					margin="133,170,0,0"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_PRESTO_SECTION}
					style={this.style_Presto_Section}>
					<LinearLayout
						id={this.idSet.PrestoHeader}
						height="72"
						width="1116"
						orientation="vertical"
						margin="2,0,0,0"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_PRESTOHEADER}
						style={this.style_PrestoHeader}>
						<TextView
							id={this.idSet.Presto_Section_Header}
							height="72"
							width="1116"
							textSize="40"
							color="#ff000000"
							fontStyle="Avenir-Black"
							lineHeight="72px"
							gravity="center"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_PRESTO_SECTION_HEADER}
							text={STR.JUSPAY_UNIVERSITY_HOME_PRESTO_SECTION_HEADER}
							style={this.style_Presto_Section_Header} />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.Logo}
						height="93"
						width="match_parent"
						orientation="horizontal"
						gravity="center_vertical"
						margin="0,95,0,0"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_LOGO}
						style={this.style_Logo}>
						<ImageView
							id={this.idSet.BHIM}
							height="93"
							width="221"
							orientation="vertical"
							imageUrl="bhim15"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_BHIM}
							style={this.style_BHIM} />
						<ImageView
							id={this.idSet.Juspay_Safe}
							height="93"
							width="244"
							orientation="vertical"
							margin="217,0,0,0"
							imageUrl="juspaysafe14"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_JUSPAY_SAFE}
							style={this.style_Juspay_Safe} />
						<LinearLayout
							id={this.idSet.Space5}
							weight="1"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE5}
							style={this.style_Space5} />
						<ImageView
							id={this.idSet.Juspay_Fuel}
							height="93"
							width="261"
							orientation="vertical"
							gravity="center_horizontal"
							imageUrl="juspayfuel13"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_JUSPAY_FUEL}
							style={this.style_Juspay_Fuel} />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.Space4}
						weight="1"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE4}
						style={this.style_Space4} />
					<LinearLayout
						id={this.idSet.LogoMessage}
						height="330"
						width="750"
						orientation="vertical"
						margin="191,0,0,0"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_LOGOMESSAGE}
						style={this.style_LogoMessage}>
						<TextView
							id={this.idSet.Hero_Story}
							height="256"
							width="match_parent"
							textSize="18"
							color="#ff3a3a3a"
							fontStyle="RobotoMono-Regular"
							lineHeight="32px"
							gravity="center"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_HERO_STORY}
							text={STR.JUSPAY_UNIVERSITY_HOME_HERO_STORY}
							style={this.style_Hero_Story} />
						<TextView
							id={this.idSet.Learn_more_about_Pre}
							height="24"
							width="249"
							margin="245,50,0,0"
							textSize="18"
							color="#ffed0000"
							fontStyle="RobotoMono-Bold"
							gravity="center"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_LEARN_MORE_ABOUT_PRE}
							text={STR.JUSPAY_UNIVERSITY_HOME_LEARN_MORE_ABOUT_PRE}
							style={this.style_Learn_more_about_Pre} />
					</LinearLayout>
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Course_Details}
					height="737"
					width="match_parent"
					orientation="vertical"
					gravity="center_horizontal"
					margin="73,204,15,0"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_COURSE_DETAILS}
					style={this.style_Course_Details}>
					<LinearLayout
						id={this.idSet.Course}
						height="72"
						width="match_parent"
						orientation="vertical"
						margin="114,0,113,0"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_COURSE}
						style={this.style_Course}>
						<TextView
							id={this.idSet.Presto_Section_Header}
							height="72"
							width="1013"
							textSize="40"
							color="#ff000000"
							fontStyle="Avenir-Black"
							lineHeight="72px"
							gravity="center"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_PRESTO_SECTION_HEADER}
							text={STR.JUSPAY_UNIVERSITY_HOME_PRESTO_SECTION_HEADER}
							style={this.style_Presto_Section_Header} />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.Space6}
						weight="1"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE6}
						style={this.style_Space6} />
					<LinearLayout
						id={this.idSet.CourseMessage}
						height="605"
						width="match_parent"
						orientation="horizontal"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_COURSEMESSAGE}
						style={this.style_CourseMessage}>
						<LinearLayout
							id={this.idSet.Curriculum}
							height="605"
							width="500"
							orientation="vertical"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_CURRICULUM}
							style={this.style_Curriculum}>
							<TextView
								id={this.idSet.Curriculum}
								height="33"
								width="122"
								textSize="24"
								color="#ffed0000"
								fontStyle="Avenir-Heavy"
								gravity="left"
								accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_CURRICULUM}
								text={STR.JUSPAY_UNIVERSITY_HOME_CURRICULUM}
								style={this.style_Curriculum} />
							<ImageView
								id={this.idSet.Open_book}
								height="120"
								width="162"
								margin="0,40,0,0"
								imageUrl="openbook12"
								accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_OPEN_BOOK}
								style={this.style_Open_book} />
							<LinearLayout
								id={this.idSet.Space8}
								weight="1"
								accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE8}
								style={this.style_Space8} />
							<TextView
								id={this.idSet.Hero_Story}
								height="352"
								width="match_parent"
								textSize="18"
								color="#ff3a3a3a"
								fontStyle="RobotoMono-Regular"
								lineHeight="32px"
								gravity="left"
								accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_HERO_STORY}
								text={STR.JUSPAY_UNIVERSITY_HOME_HERO_STORY}
								style={this.style_Hero_Story} />
						</LinearLayout>
						<LinearLayout
							id={this.idSet.Space7}
							weight="1"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE7}
							style={this.style_Space7} />
						<LinearLayout
							id={this.idSet.Project}
							height="602"
							width="520"
							orientation="vertical"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_PROJECT}
							style={this.style_Project}>
							<LinearLayout
								id={this.idSet.Project}
								height="349"
								width="match_parent"
								orientation="vertical"
								accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_PROJECT}
								style={this.style_Project}>
								<TextView
									id={this.idSet.Project}
									height="33"
									width="80"
									textSize="24"
									color="#ffed0000"
									fontStyle="Avenir-Heavy"
									gravity="left"
									accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_PROJECT}
									text={STR.JUSPAY_UNIVERSITY_HOME_PROJECT}
									style={this.style_Project} />
								<ImageView
									id={this.idSet.School_material}
									height="120"
									width="120"
									padding="0,0,-1,0"
									margin="0,40,0,0"
									imageUrl="schoolmaterial11"
									accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SCHOOL_MATERIAL}
									style={this.style_School_material} />
								<LinearLayout
									id={this.idSet.Space10}
									weight="1"
									accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE10}
									style={this.style_Space10} />
								<TextView
									id={this.idSet.Hero_Story_Copy_3}
									height="96"
									width="match_parent"
									textSize="18"
									color="#ff3a3a3a"
									fontStyle="RobotoMono-Regular"
									lineHeight="32px"
									gravity="left"
									accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_HERO_STORY_COPY_3}
									text={STR.JUSPAY_UNIVERSITY_HOME_HERO_STORY_COPY_3}
									style={this.style_Hero_Story_Copy_3} />
							</LinearLayout>
							<LinearLayout
								id={this.idSet.Space9}
								weight="1"
								accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE9}
								style={this.style_Space9} />
							<LinearLayout
								id={this.idSet.Forum}
								height="193"
								width="511"
								orientation="vertical"
								accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_FORUM}
								style={this.style_Forum}>
								<TextView
									id={this.idSet.Forum}
									height="33"
									width="73"
									textSize="24"
									color="#ffed0000"
									fontStyle="Avenir-Heavy"
									gravity="left"
									accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_FORUM}
									text={STR.JUSPAY_UNIVERSITY_HOME_FORUM}
									style={this.style_Forum} />
								<LinearLayout
									id={this.idSet.Group_4}
									height="120"
									width="match_parent"
									orientation="horizontal"
									margin="0,40,0,0"
									accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_GROUP_4}
									style={this.style_Group_4}>
									<ImageView
										id={this.idSet.Chat__1_}
										height="120"
										width="120"
										padding="0,0,1,1"
										imageUrl="chat110"
										accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_CHAT__1_}
										style={this.style_Chat__1_} />
									<LinearLayout
										id={this.idSet.Space11}
										weight="1"
										accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE11}
										style={this.style_Space11} />
									<TextView
										id={this.idSet.Hero_Story_Copy_4}
										height="96"
										width="331"
										margin="0,24,0,0"
										textSize="18"
										color="#ff3a3a3a"
										fontStyle="RobotoMono-Regular"
										lineHeight="32px"
										gravity="left"
										accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_HERO_STORY_COPY_4}
										text={STR.JUSPAY_UNIVERSITY_HOME_HERO_STORY_COPY_4}
										style={this.style_Hero_Story_Copy_4} />
								</LinearLayout>
							</LinearLayout>
						</LinearLayout>
					</LinearLayout>
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Group_3}
					height="508"
					width="match_parent"
					orientation="vertical"
					padding="0,125,0,113"
					margin="73,25,15,0"
					background="#ffffffff"
					cornerRadius="0"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_GROUP_3}
					style={this.style_Group_3}>
					<LinearLayout
						id={this.idSet.Group_2}
						height="270"
						width="1240"
						orientation="horizontal"
						gravity="center_vertical"
						padding="100,95,100,95"
						background="#ffee0000"
						cornerRadius="0"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_GROUP_2}
						style={this.style_Group_2}>
						<TextView
							id={this.idSet.Sounds_exciting__The}
							height="80"
							width="577"
							textSize="24"
							color="#fffefffe"
							fontStyle="RobotoMono-Regular"
							lineHeight="40px"
							gravity="left"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SOUNDS_EXCITING__THE}
							text={STR.JUSPAY_UNIVERSITY_HOME_SOUNDS_EXCITING__THE}
							style={this.style_Sounds_exciting__The} />
						<LinearLayout
							id={this.idSet.Space12}
							weight="1"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_SPACE12}
							style={this.style_Space12} />
						<LinearLayout
							id={this.idSet.Start_Button}
							height="60"
							width="240"
							orientation="vertical"
							padding="48,9,48,10"
							background="#ffffffff"
							cornerRadius="4"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_START_BUTTON}
							style={this.style_Start_Button}>
							<TextView
								id={this.idSet.Let_s_Start}
								height="41"
								width="144"
								textSize="30"
								color="#ffed0000"
								fontStyle="Avenir-Heavy"
								gravity="center"
								accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_LET_S_START}
								text={STR.JUSPAY_UNIVERSITY_HOME_LET_S_START}
								style={this.style_Let_s_Start} />
						</LinearLayout>
					</LinearLayout>
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = Juspay_University_Home;
