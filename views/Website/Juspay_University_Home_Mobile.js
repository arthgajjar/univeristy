const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const ImageView = require("@juspay/mystique-backend").views.ImageView;
const TextView = require("@juspay/mystique-backend").views.TextView;

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Website/Juspay_University_Home_Mobile');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class Juspay_University_Home_Mobile extends Controller {

	constructor(props, children, state) {
		super(props, children);
		this.shouldCacheScreen = true;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				padding="0,29,0,51"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				clickable="true"
				accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_8}
				style={this.style_Group_8}>
				<ImageView
					id={this.idSet.Group_7}
					height="21"
					width="183"
					orientation="vertical"
					padding="0,0,20,0"
					margin="177,0,0,0"
					imageUrl="group72"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_7}
					style={this.style_Group_7} />
				<ImageView
					id={this.idSet.Group_5}
					height="258"
					width="300"
					margin="30,29,0,0"
					imageUrl="group51"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_5}
					style={this.style_Group_5} />
				<TextView
					id={this.idSet.Hero_Epic}
					height="80"
					width="match_parent"
					margin="20,8,20,0"
					textSize="30"
					color="#ff000000"
					fontStyle="Avenir-Black"
					lineHeight="40px"
					gravity="left"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_EPIC}
					text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_EPIC}
					style={this.style_Hero_Epic} />
				<TextView
					id={this.idSet.Section_body}
					height="150"
					width="match_parent"
					margin="20,30,20,0"
					textSize="12"
					color="#ff3a3a3a"
					fontStyle="RobotoMono-Regular"
					lineHeight="25px"
					gravity="left"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_SECTION_BODY}
					text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_SECTION_BODY}
					style={this.style_Section_body} />
				<TextView
					id={this.idSet.Jump_in___get_starte}
					height="25"
					width="match_parent"
					margin="20,30,20,0"
					textSize="12"
					color="#ffee0000"
					fontStyle="RobotoMono-Bold"
					lineHeight="25px"
					gravity="left"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_JUMP_IN___GET_STARTE}
					text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_JUMP_IN___GET_STARTE}
					style={this.style_Jump_in___get_starte} />
				<LinearLayout
					id={this.idSet.Group_6}
					height="667"
					width="match_parent"
					orientation="vertical"
					gravity="center_horizontal"
					padding="0,30,0,39"
					margin="0,68,0,0"
					background="#19bbbbbb"
					cornerRadius="0"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_6}
					style={this.style_Group_6}>
					<LinearLayout
						id={this.idSet.PrestoHeader}
						height="120"
						width="match_parent"
						orientation="vertical"
						margin="20,0,20,0"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_PRESTOHEADER}
						style={this.style_PrestoHeader}>
						<TextView
							id={this.idSet.Presto_Section_Header}
							height="120"
							width="320"
							textSize="30"
							color="#ff000000"
							fontStyle="Avenir-Black"
							lineHeight="40px"
							gravity="left"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_PRESTO_SECTION_HEADER}
							text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_PRESTO_SECTION_HEADER}
							style={this.style_Presto_Section_Header} />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.Logo}
						height="70"
						width="match_parent"
						orientation="horizontal"
						padding="0,0,5,0"
						margin="20,32,20,0"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_LOGO}
						style={this.style_Logo}>
						<ImageView
							id={this.idSet.BHIM}
							height="50"
							width="72"
							orientation="vertical"
							margin="0,6,0,0"
							imageUrl="bhim9"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_BHIM}
							style={this.style_BHIM} />
						<ImageView
							id={this.idSet.Juspay_Safe}
							height="64"
							width="133"
							orientation="vertical"
							padding="0,0,-1,2"
							margin="18,6,0,0"
							imageUrl="juspaysafe8"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_JUSPAY_SAFE}
							style={this.style_Juspay_Safe} />
						<ImageView
							id={this.idSet.Juspay_Fuel}
							height="61"
							width="78"
							orientation="vertical"
							padding="0,0,-1,0"
							margin="18,0,0,0"
							imageUrl="juspayfuel7"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_JUSPAY_FUEL}
							style={this.style_Juspay_Fuel} />
					</LinearLayout>
					<TextView
						id={this.idSet.Hero_Story}
						height="300"
						width="match_parent"
						margin="20,30,20,0"
						textSize="12"
						color="#ff3a3a3a"
						fontStyle="RobotoMono-Regular"
						lineHeight="25px"
						gravity="left"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY}
						text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY}
						style={this.style_Hero_Story} />
					<TextView
						id={this.idSet.Learn_more_about_Pre}
						height="16"
						width="match_parent"
						margin="0,30,0,0"
						textSize="12"
						color="#ffed0000"
						fontStyle="RobotoMono-Bold"
						gravity="center"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_LEARN_MORE_ABOUT_PRE}
						text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_LEARN_MORE_ABOUT_PRE}
						style={this.style_Learn_more_about_Pre} />
				</LinearLayout>
				<TextView
					id={this.idSet.Presto_Section_Header}
					height="80"
					width="match_parent"
					margin="20,30,20,0"
					textSize="30"
					color="#ff000000"
					fontStyle="Avenir-Black"
					lineHeight="40px"
					gravity="left"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_PRESTO_SECTION_HEADER}
					text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_PRESTO_SECTION_HEADER}
					style={this.style_Presto_Section_Header} />
				<TextView
					id={this.idSet.Curriculum}
					height="20"
					width="match_parent"
					margin="0,60,0,0"
					textSize="15"
					color="#ffed0000"
					fontStyle="Avenir-Heavy"
					gravity="center"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_CURRICULUM}
					text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_CURRICULUM}
					style={this.style_Curriculum} />
				<ImageView
					id={this.idSet.Open_book}
					height="120"
					width="160"
					margin="100,30,0,0"
					imageUrl="openbook6"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_OPEN_BOOK}
					style={this.style_Open_book} />
				<TextView
					id={this.idSet.Hero_Story}
					height="400"
					width="match_parent"
					margin="20,30,20,0"
					textSize="18"
					color="#ff3a3a3a"
					fontStyle="RobotoMono-Regular"
					lineHeight="25px"
					gravity="left"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY}
					text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY}
					style={this.style_Hero_Story} />
				<TextView
					id={this.idSet.Project}
					height="20"
					width="match_parent"
					margin="0,60,0,0"
					textSize="15"
					color="#ffed0000"
					fontStyle="Avenir-Heavy"
					gravity="center"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_PROJECT}
					text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_PROJECT}
					style={this.style_Project} />
				<ImageView
					id={this.idSet.School_material}
					height="61"
					width="64"
					margin="148,30,0,0"
					imageUrl="schoolmaterial5"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_SCHOOL_MATERIAL}
					style={this.style_School_material} />
				<TextView
					id={this.idSet.Hero_Story_Copy_3}
					height="100"
					width="match_parent"
					margin="20,30,20,0"
					textSize="18"
					color="#ff3a3a3a"
					fontStyle="RobotoMono-Regular"
					lineHeight="25px"
					gravity="left"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY_COPY_3}
					text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY_COPY_3}
					style={this.style_Hero_Story_Copy_3} />
				<LinearLayout
					id={this.idSet.Group_3}
					height="125"
					width="match_parent"
					orientation="horizontal"
					margin="20,60,20,0"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_3}
					style={this.style_Group_3}>
					<LinearLayout
						id={this.idSet.Group_2}
						height="101"
						width="70"
						orientation="vertical"
						gravity="center_horizontal"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_2}
						style={this.style_Group_2}>
						<TextView
							id={this.idSet.Forum}
							height="16"
							width="match_parent"
							textSize="12"
							color="#ffed0000"
							fontStyle="Avenir-Heavy"
							gravity="left"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_FORUM}
							text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_FORUM}
							style={this.style_Forum} />
						<ImageView
							id={this.idSet.Chat__1_}
							height="70"
							width="70"
							margin="0,15,0,0"
							imageUrl="chat14"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_CHAT__1_}
							style={this.style_Chat__1_} />
					</LinearLayout>
					<TextView
						id={this.idSet.Hero_Story_Copy_4}
						height="125"
						width="200"
						margin="50,0,0,0"
						weight="1"
						textSize="18"
						color="#ff3a3a3a"
						fontStyle="RobotoMono-Regular"
						lineHeight="25px"
						gravity="left"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY_COPY_4}
						text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY_COPY_4}
						style={this.style_Hero_Story_Copy_4} />
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Group_4}
					height="270"
					width="match_parent"
					orientation="vertical"
					gravity="center_horizontal"
					padding="20,31,20,38"
					margin="0,50,0,0"
					background="#ffee0000"
					cornerRadius="0"
					accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_4}
					style={this.style_Group_4}>
					<TextView
						id={this.idSet.Sounds_exciting__The}
						height="50"
						width="match_parent"
						textSize="12"
						color="#fffefffe"
						fontStyle="RobotoMono-Regular"
						lineHeight="25px"
						gravity="left"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_SOUNDS_EXCITING__THE}
						text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_SOUNDS_EXCITING__THE}
						style={this.style_Sounds_exciting__The} />
					<LinearLayout
						id={this.idSet.Space1}
						weight="1"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_SPACE1}
						style={this.style_Space1} />
					<LinearLayout
						id={this.idSet.Start_Button}
						height="60"
						width="match_parent"
						orientation="vertical"
						padding="48,9,48,10"
						margin="40,0,40,0"
						background="#ffffffff"
						cornerRadius="4"
						accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_START_BUTTON}
						style={this.style_Start_Button}>
						<TextView
							id={this.idSet.Let_s_Start}
							height="41"
							width="144"
							textSize="30"
							color="#ffed0000"
							fontStyle="Avenir-Heavy"
							gravity="center"
							accessibilityHint={HINT.JUSPAY_UNIVERSITY_HOME_MOBILE_LET_S_START}
							text={STR.JUSPAY_UNIVERSITY_HOME_MOBILE_LET_S_START}
							style={this.style_Let_s_Start} />
					</LinearLayout>
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = Juspay_University_Home_Mobile;
