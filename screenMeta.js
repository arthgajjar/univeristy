const RootScreen = require("./views/RootScreen");
// Screens of page: Website
const Juspay_University_Home_Mobile = require("./views/Website/Juspay_University_Home_Mobile");
const Juspay_University_Home = require("./views/Website/Juspay_University_Home");

const screens = {
	Juspay_University_Home_Mobile,
	Juspay_University_Home,
	RootScreen
};

const INIT_UI = "Juspay_University_Home_Mobile";

module.exports = {
	screens,
	INIT_UI
};
