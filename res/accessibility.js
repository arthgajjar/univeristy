const DEFAULT = "en_US";

const strings = {
  "en_US": {
    "JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_8": "Group 8",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_7": "Group 7",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_5": "Group 5",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_EPIC": "Hero Epic",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_SECTION_BODY": "Section body",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_JUMP_IN___GET_STARTE": "Jump in & get starte",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_6": "Group 6",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_PRESTOHEADER": "PrestoHeader",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_PRESTO_SECTION_HEADER": "Presto Section Header",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_LOGO": "Logo",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_BHIM": "BHIM",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_JUSPAY_SAFE": "Juspay Safe",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_JUSPAY_FUEL": "Juspay Fuel",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY": "Hero Story",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_LEARN_MORE_ABOUT_PRE": "Learn more about Pre",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_CURRICULUM": "Curriculum",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_OPEN_BOOK": "open-book",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_PROJECT": "Project",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_SCHOOL_MATERIAL": "school-material",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY_COPY_3": "Hero Story Copy 3",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_3": "Group 3",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_2": "Group 2",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_FORUM": "Forum",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_CHAT__1_": "chat (1)",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY_COPY_4": "Hero Story Copy 4",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_GROUP_4": "Group 4",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_SOUNDS_EXCITING__THE": "Sounds exciting? The",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_SPACE1": "space1",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_START_BUTTON": "Start Button",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_LET_S_START": "Let’s Start",
    "JUSPAY_UNIVERSITY_HOME_JUSPAYUNIVERSITY": "JuspayUniversity",
    "JUSPAY_UNIVERSITY_HOME_HEADER": "Header",
    "JUSPAY_UNIVERSITY_HOME_LOGOLEFT": "LogoLeft",
    "JUSPAY_UNIVERSITY_HOME_SPACE2": "space2",
    "JUSPAY_UNIVERSITY_HOME_HEADERRIGHT": "HeaderRight",
    "JUSPAY_UNIVERSITY_HOME_PROJECTS": "Projects",
    "JUSPAY_UNIVERSITY_HOME_FORUM": "Forum",
    "JUSPAY_UNIVERSITY_HOME_SPACE3": "space3",
    "JUSPAY_UNIVERSITY_HOME_START_BUTTON": "Start Button",
    "JUSPAY_UNIVERSITY_HOME_LET_S_START": "Let’s Start",
    "JUSPAY_UNIVERSITY_HOME_HERO___ABOVE_THE_FOLD": "Hero - above the fold",
    "JUSPAY_UNIVERSITY_HOME_INTRO_VIDEO": "Intro Video",
    "JUSPAY_UNIVERSITY_HOME_INTRORIGHT": "IntroRight",
    "JUSPAY_UNIVERSITY_HOME_HERO_EPIC": "Hero Epic",
    "JUSPAY_UNIVERSITY_HOME_SECTION_BODY": "Section body",
    "JUSPAY_UNIVERSITY_HOME_PRESTO_SECTION": "Presto Section",
    "JUSPAY_UNIVERSITY_HOME_PRESTOHEADER": "PrestoHeader",
    "JUSPAY_UNIVERSITY_HOME_PRESTO_SECTION_HEADER": "Presto Section Header",
    "JUSPAY_UNIVERSITY_HOME_LOGO": "Logo",
    "JUSPAY_UNIVERSITY_HOME_BHIM": "BHIM",
    "JUSPAY_UNIVERSITY_HOME_JUSPAY_SAFE": "Juspay Safe",
    "JUSPAY_UNIVERSITY_HOME_SPACE5": "space5",
    "JUSPAY_UNIVERSITY_HOME_JUSPAY_FUEL": "Juspay Fuel",
    "JUSPAY_UNIVERSITY_HOME_SPACE4": "space4",
    "JUSPAY_UNIVERSITY_HOME_LOGOMESSAGE": "LogoMessage",
    "JUSPAY_UNIVERSITY_HOME_HERO_STORY": "Hero Story",
    "JUSPAY_UNIVERSITY_HOME_LEARN_MORE_ABOUT_PRE": "Learn more about Pre",
    "JUSPAY_UNIVERSITY_HOME_COURSE_DETAILS": "Course Details",
    "JUSPAY_UNIVERSITY_HOME_COURSE": "course",
    "JUSPAY_UNIVERSITY_HOME_SPACE6": "space6",
    "JUSPAY_UNIVERSITY_HOME_COURSEMESSAGE": "courseMessage",
    "JUSPAY_UNIVERSITY_HOME_CURRICULUM": "Curriculum",
    "JUSPAY_UNIVERSITY_HOME_OPEN_BOOK": "open-book",
    "JUSPAY_UNIVERSITY_HOME_SPACE8": "space8",
    "JUSPAY_UNIVERSITY_HOME_SPACE7": "space7",
    "JUSPAY_UNIVERSITY_HOME_PROJECT": "Project",
    "JUSPAY_UNIVERSITY_HOME_SCHOOL_MATERIAL": "school-material",
    "JUSPAY_UNIVERSITY_HOME_SPACE10": "space10",
    "JUSPAY_UNIVERSITY_HOME_HERO_STORY_COPY_3": "Hero Story Copy 3",
    "JUSPAY_UNIVERSITY_HOME_SPACE9": "space9",
    "JUSPAY_UNIVERSITY_HOME_GROUP_4": "Group 4",
    "JUSPAY_UNIVERSITY_HOME_CHAT__1_": "chat (1)",
    "JUSPAY_UNIVERSITY_HOME_SPACE11": "space11",
    "JUSPAY_UNIVERSITY_HOME_HERO_STORY_COPY_4": "Hero Story Copy 4",
    "JUSPAY_UNIVERSITY_HOME_GROUP_3": "Group 3",
    "JUSPAY_UNIVERSITY_HOME_GROUP_2": "Group 2",
    "JUSPAY_UNIVERSITY_HOME_SOUNDS_EXCITING__THE": "Sounds exciting? The",
    "JUSPAY_UNIVERSITY_HOME_SPACE12": "space12",
    "JUSPAY_LOGO_V3_JUSPAY_LOGO_V3": "Juspay_Logo_V3",
    "JUSPAY_LOGO_V3_LAYER_3": "Layer_3",
    "JUSPAY_LOGO_V3_GROUP": "Group",
    "JUSPAY_LOGO_V3_SHAPE": "Shape"
  }
}

module.exports = () => Object.assign({}, strings[DEFAULT], strings[window.LANGUAGE]);
