const DEFAULT = "en_US";

const strings = {
  "en_US": {
    "JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_EPIC": "Build real products\nwhile in college.",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_SECTION_BODY": "We want to enable you to become a true Product Maker. We’ll help you learn Functional Programming, Category Theory and Product Design. The hard fundamentals, once learnt, shape you to create serious, real-world, large scale applications.",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_JUMP_IN___GET_STARTE": "Jump in & get started.",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_PRESTO_SECTION_HEADER": "What you’ll do in this course",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY": "The course provides a clear path to learn:\n\n- Quick basics of Programming\n- Understand Functional Programming concepts\n- Define lego blocks of app development\n- Explore the Presto Framework\n- Apply Presto to develop a working app\n\nWe’ll be continually updating the curriculum with relevant examples and links.",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_LEARN_MORE_ABOUT_PRE": "Learn more about Presto",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_CURRICULUM": "Curriculum",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_PROJECT": "Project",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY_COPY_3": "Upon completion, you can choose to submit this project to Juspay. Your submission can get you hired at Juspay.",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_FORUM": "Forum",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_HERO_STORY_COPY_4": "The go-to place to ask n’ answer questions, seek feedback and help other geeks. ",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_SOUNDS_EXCITING__THE": "Sounds exciting? Then let’s get learning\nand make something kick-ass.",
    "JUSPAY_UNIVERSITY_HOME_MOBILE_LET_S_START": "Let’s Start",
    "JUSPAY_UNIVERSITY_HOME_PROJECTS": "Projects",
    "JUSPAY_UNIVERSITY_HOME_FORUM": "Forum",
    "JUSPAY_UNIVERSITY_HOME_LET_S_START": "Let’s Start",
    "JUSPAY_UNIVERSITY_HOME_HERO_EPIC": "Build real products\nwhile in college.",
    "JUSPAY_UNIVERSITY_HOME_SECTION_BODY": "We want to enable you to become a true Product Maker. We’ll help you learn Functional Programming, Category Theory and Product Design. The hard fundamentals, once learnt, shape you to create serious, real-world, large scale applications. \n\nJump in & get started.",
    "JUSPAY_UNIVERSITY_HOME_PRESTO_SECTION_HEADER": "What you’ll do in this course",
    "JUSPAY_UNIVERSITY_HOME_HERO_STORY": "The course provides a clear path to learn:\n\n- Quick basics of Programming\n- Understand Functional Programming concepts\n- Define lego blocks of app development\n- Explore the Presto Framework\n- Apply Presto to develop a working app\n\n\nWe’ll be continually updating the curriculum with relevant examples and links.",
    "JUSPAY_UNIVERSITY_HOME_LEARN_MORE_ABOUT_PRE": "Learn more about Presto",
    "JUSPAY_UNIVERSITY_HOME_CURRICULUM": "Curriculum",
    "JUSPAY_UNIVERSITY_HOME_PROJECT": "Project",
    "JUSPAY_UNIVERSITY_HOME_HERO_STORY_COPY_3": "Upon completion, you can choose to submit this project to Juspay. Your submission can get you hired at Juspay.",
    "JUSPAY_UNIVERSITY_HOME_HERO_STORY_COPY_4": "The go-to place to ask n’ answer questions, seek feedback and help other geeks. ",
    "JUSPAY_UNIVERSITY_HOME_SOUNDS_EXCITING__THE": "Sounds exciting? Then let’s get learning\nand make something kick-ass."
  }
}

module.exports = () => Object.assign({}, strings[DEFAULT], strings[window.LANGUAGE]);
