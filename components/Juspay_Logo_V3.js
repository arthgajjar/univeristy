const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;

const Config = require('./../globalConfig');
const Controller = require('./../controller/components/Juspay_Logo_V3');
const Strings = require('./../res/strings');
const Accessibility = require('./../res/accessibility');

let STR = {};
let HINT = {};

class Juspay_Logo_V3 extends Controller {

	constructor(props, children, state) {
		super(props, children);
		this.shouldCacheScreen = true;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				id={this.idSet.Juspay_Logo_V3}
				height={this.props.height ? this.props.height:"75"}
				width={this.props.width ? this.props.width:"339"}
				orientation="vertical"
				root={true}
				clickable="true"
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.JUSPAY_LOGO_V3_JUSPAY_LOGO_V3}
				style={this.style_Juspay_Logo_V3}>
				<LinearLayout
					id={this.idSet.Layer_3}
					height="75"
					width="339"
					background="#ff000000"
					accessibilityHint={HINT.JUSPAY_LOGO_V3_LAYER_3}
					style={this.style_Layer_3}>
					<LinearLayout
						id={this.idSet.Group}
						height="74"
						width="73"
						background="#ff000000"
						accessibilityHint={HINT.JUSPAY_LOGO_V3_GROUP}
						style={this.style_Group}>
						<LinearLayout
							id={this.idSet.Shape}
							height="72"
							width="36"
							background="#bfee0000"
							cornerRadius="0"
							accessibilityHint={HINT.JUSPAY_LOGO_V3_SHAPE}
							style={this.style_Shape} />
						<LinearLayout
							id={this.idSet.Shape}
							height="72"
							width="36"
							background="#ffee0000"
							cornerRadius="0"
							accessibilityHint={HINT.JUSPAY_LOGO_V3_SHAPE}
							style={this.style_Shape} />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.Group}
						height="55"
						width="256"
						background="#ff000000"
						accessibilityHint={HINT.JUSPAY_LOGO_V3_GROUP}
						style={this.style_Group}>
						<LinearLayout
							id={this.idSet.Shape}
							height="52"
							width="30"
							background="#ff444444"
							cornerRadius="0"
							accessibilityHint={HINT.JUSPAY_LOGO_V3_SHAPE}
							style={this.style_Shape} />
						<LinearLayout
							id={this.idSet.Shape}
							height="52"
							width="41"
							background="#ff444444"
							cornerRadius="0"
							accessibilityHint={HINT.JUSPAY_LOGO_V3_SHAPE}
							style={this.style_Shape} />
						<LinearLayout
							id={this.idSet.Shape}
							height="54"
							width="35"
							background="#ff444444"
							cornerRadius="0"
							accessibilityHint={HINT.JUSPAY_LOGO_V3_SHAPE}
							style={this.style_Shape} />
						<LinearLayout
							id={this.idSet.Shape}
							height="51"
							width="36"
							background="#ff444444"
							cornerRadius="0"
							accessibilityHint={HINT.JUSPAY_LOGO_V3_SHAPE}
							style={this.style_Shape} />
						<LinearLayout
							id={this.idSet.Shape}
							height="51"
							width="52"
							background="#ff444444"
							cornerRadius="0"
							accessibilityHint={HINT.JUSPAY_LOGO_V3_SHAPE}
							style={this.style_Shape} />
						<LinearLayout
							id={this.idSet.Shape}
							height="51"
							width="48"
							background="#ff444444"
							cornerRadius="0"
							accessibilityHint={HINT.JUSPAY_LOGO_V3_SHAPE}
							style={this.style_Shape} />
					</LinearLayout>
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = Juspay_Logo_V3;
