const meta = require('./screenMeta');
const purescriptObj = require('./output/Main/index.js');
const init = require("@juspay/mystique-backend").init;

window.pages = require('./screens').pages;
init(meta, purescriptObj);